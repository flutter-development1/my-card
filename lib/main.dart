import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            title: Text('My Business Card'),
            backgroundColor: Colors.cyan,
          ),
          floatingActionButton: FloatingActionButton(
            child: Icon(Icons.arrow_forward),
            onPressed: () {
              NextPage();
            },
          ),
          body: SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                  radius: 50.0,
                  backgroundImage: AssetImage('images/fb_pic.jpg'),
                ),
                SizedBox(
                  height: 10,
                ),
                Text('Gopinath Sreenivasan',
                    style: TextStyle(fontFamily: 'Pacifico')),
                Text('FLUTTER DEVELOPER',
                    style: TextStyle(fontFamily: 'SourceSansPro')),
                SizedBox(
                  height: 10,
                ),
                Container(
                    color: Colors.blue,
                    margin: EdgeInsets.only(left: 40, right: 40),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.phone_android),
                        Text(
                          '+46-12345678',
                          style: TextStyle(
                              fontFamily: 'SourceSansPro', letterSpacing: 2),
                        ),
                      ],
                    )),
                SizedBox(
                  height: 2,
                ),
                Container(
                    color: Colors.blue,
                    margin: EdgeInsets.only(left: 40, right: 40),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(Icons.email),
                        Text(
                          'learningflutter@email.com',
                          style: TextStyle(
                              fontFamily: 'SourceSansPro', letterSpacing: 0),
                        ),
                      ],
                    ))
              ],
            ),
          )),
    );
  }
}

class NextPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Text('Next Page'),
    );
  }
}
